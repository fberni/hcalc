{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE GADTs #-}

module HCalc
  ( loop
  ) where

import Prelude hiding (div)

import Control.Applicative (Alternative(..), liftA2)
import Control.Monad (ap, unless, void)
import Data.Char (isDigit, isSpace)
import Data.Functor (($>))
import System.IO (isEOF)
import Text.Read (readMaybe)

data Operator
  = Plus
  | Minus
  | Multiplication
  | Division
  | Power
  deriving (Show, Eq)

data Token
  = TokOp Operator
  | TokNum Double
  | TokOpenParen
  | TokCloseParen
  | TokEof
  deriving (Show, Eq)

data Expr
  = Add Expr Expr
  | Sub Expr Expr
  | Mul Expr Expr
  | Div Expr Expr
  | Pow Expr Expr
  | Neg Expr
  | Num Double
  deriving (Show)

newtype Parser a =
  P
    { runP :: [Token] -> Maybe (a, [Token])
    }
  deriving (Functor)

parse :: Parser a -> [Token] -> Maybe a
parse (P p) toks = do
  x <- p toks
  return $ fst x

instance Applicative Parser where
  pure = return
  (<*>) = ap

instance Monad Parser where
  return x = P $ \inp -> Just (x, inp)
  (P p) >>= k =
    P $ \inp -> do
      (x, inp') <- p inp
      runP (k x) inp'

instance Alternative Parser where
  empty = P $ const Nothing
  (P p) <|> (P q) = P $ liftA2 (<|>) p q

tokenize :: String -> Maybe [Token]
tokenize [] = return [TokEof]
tokenize l@(c:cs)
  | c == '+' = helper $ TokOp Plus
  | c == '-' = helper $ TokOp Minus
  | c == '*' = helper $ TokOp Multiplication
  | c == '/' = helper $ TokOp Division
  | c == '^' = helper $ TokOp Power
  | c == '(' = helper TokOpenParen
  | c == ')' = helper TokCloseParen
  | isSpace c = tokenize cs
  | otherwise = do
    n <- readMaybe num
    ts <- tokenize rest
    return $ TokNum n : ts
  where
    helper t = (:) <$> Just t <*> tokenize cs
    (num, rest) = span (\x -> isDigit x || x == '.') l

tok :: Token -> Parser Token
tok t =
  P $ \case
    (t':ts) ->
      if t == t'
        then Just (t, ts)
        else Nothing
    [] -> Nothing

eof :: Parser ()
eof = tok TokEof $> ()

val :: Parser Expr
val =
  P $ \case
    ((TokNum n):ts) -> Just (Num n, ts)
    _ -> Nothing

plus :: Parser Operator
plus = Plus <$ tok (TokOp Plus)

minus :: Parser Operator
minus = Minus <$ tok (TokOp Minus)

mult :: Parser Operator
mult = Multiplication <$ tok (TokOp Multiplication)

div :: Parser Operator
div = Division <$ tok (TokOp Division)

addBinop :: Parser Operator
addBinop = plus <|> minus

multBinop :: Parser Operator
multBinop = mult <|> div

inParens :: Parser a -> Parser a
inParens p = tok TokOpenParen *> p <* tok TokCloseParen

simple :: Parser Expr
simple = inParens expr <|> val

power :: Parser Expr
power =
  do x <- simple
     void $ tok $ TokOp Power
     Pow x <$> factor
     <|> simple

factor :: Parser Expr
factor = power <|> minus *> (Neg <$> factor)

term :: Parser Expr
term = do
  v <- factor
  vs <- many $ liftA2 (,) multBinop factor
  return $
    foldl
      (\x (op, y) ->
         case op of
           Multiplication -> Mul x y
           Division -> Div x y)
      v
      vs

expr :: Parser Expr
expr = do
  v <- term
  vs <- many $ liftA2 (,) addBinop term
  return $
    foldl
      (\x (op, y) ->
         case op of
           Plus -> Add x y
           Minus -> Sub x y)
      v
      vs

eval :: Expr -> Double
eval (Add x y) = eval x + eval y
eval (Sub x y) = eval x - eval y
eval (Mul x y) = eval x * eval y
eval (Div x y) = eval x / eval y
eval (Pow x y) = eval x ** eval y
eval (Neg x) = negate $ eval x
eval (Num x) = x

loop :: IO ()
loop = do
  done <- isEOF
  unless done $ do
    input <- getLine
    putStrLn $
      maybe "ERROR: invalid expression" (show . eval) $ do
        toks <- tokenize input
        parse (expr <* eof) toks
    loop
