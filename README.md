# HCalc

A simple command line calculator written in Haskell.

## Getting Started
To install `hcalc` do

```shell
$ git clone git@gitlab.com:fberni/hcalc.git hcalc
$ cd hcalc
$ cabal install
```

### Example
```shell
$ hcalc
Welcome to HCalc!
45+24
69.0
^D
Exiting...
```
