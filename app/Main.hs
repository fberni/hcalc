module Main where

import System.IO (BufferMode(NoBuffering), hSetBuffering, stdout)

import HCalc (loop)

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  putStrLn "Welcome to HCalc!"
  loop
  putStrLn "Exiting..."
